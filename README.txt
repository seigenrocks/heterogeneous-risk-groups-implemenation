
------------------------------------------------------------------------
Author: Gijsbert Schröder
Contact: gijsschroder at gmail dot com
License: GPL3 (see LICENSE.txt)
------------------------------------------------------------------------

This R code contains an implementation of the model described in:

Rozhnova G, van der Loeff MFS, Heijne JCM, Kretzschmar ME (2016) Impact
of Heterogeneity in Sexual Behavior on Effectiveness in Reducing HIV
Transmission with Test-and-Treat Strategy. PLoS Comput Biol 12(8):
e1005012. doi:10.1371/journal.pcbi.1005012


------------------------------------------------------------------------
1) INSTALLATION NOTES
------------------------------------------------------------------------

Requires R to have deSolve installed.


------------------------------------------------------------------------
2) USAGE
------------------------------------------------------------------------

Running the model requires a set of parameters and an initial state.
These R files try to optimize for the following flow:

- Create parameters
- Create initial state
- Run the model for some in-model time
- Analyze model output
- Plot results

The model requires a large number of parameters to be set, so the
default set is provided as defaultParams and some auxiliary functions
that add parameters to the parameter in a structered way are provided in
params.R.

An initial state vector may be obtained through the use of the
initialState function.

run.R contains functions for running the model for a fixed amount of
time and until equilibriation.

plot.R contains a useful plotting function. R itself contains the rest.

For detailed examples see the R files in ./examples/


------------------------------------------------------------------------
3) DATA FORMATS
------------------------------------------------------------------------

There are several data formats that are used. These are: 

a) Parameters lists
    These are lists where attributes correspond to parameters in the
model. Use unspecifiedParams to check whether all the parameters are
given. The parameters rho, gamma, and h should have the same vector
length as the number of stages. The parameters q, c, and tau should have
the same vector length as the number of risk groups.

b) State vectors
    deSolve uses these to pass state to the derivative function and
parses these as the result of the derivative function. In this context
the diffent values in the vector should line up which each other, so
that state corresponds with derivative.

c) Data matrices with time as slowest moving index
    Functions in run.R use this as their output. The output is a list
with named attributes, which contain arrays that have their dimensions
in the following order:
    - Risk group, if applicable
    - Stage, if applicable
    - Timestep

d) Data matrices with time as row
    Functions in analysis.R use this as their output and the previous as
input. The output is a matrix that has time as rows and useful data as
columns. This output format is well suited for matplot or plotData.


------------------------------------------------------------------------
4) SOURCE FILES
------------------------------------------------------------------------

./src contains the R source files, which are: 
analysis.R
params.R
plot.R
run.R
system.R
weibull.R
util.R

analysis.R contains functions that perform some analysis on integration
results. They accept input in the format provided by functions from
run.R. These functions are: riskGroupProportions and
proportionInfectedOrTreated.

params.R contains the default set of parameters and some utility
functions for testing whether a set of parameters are sane (whether
everything that needs to be defined is defined and whether parameter
dimensions match each other). Furthermore it contains some functions
that are useful for extending the default set of parameters, as is
demonstrated in main.R. Not all parameters that are required are defined
in the default set of parameters, so some use of these functions is
always necessary.

plot.R contains a useful plotting function.

run.R contains functions that run the model. These functions return data
as matrices with time as the slowest moving index.

system.R contains an implementation of the model and basic reproduction 
rate computation.

util.R contains utility functions.

weibull.R contains functions that help params.R to find values for the c
parameter given the weibull shape and mean, and risk group proportions.
Integration is necessary for finding the means. After experimentation it
was found that the DASPK is stable. Watch out for whether any stray NANs
remain for your parameters (I do not expect them to).

