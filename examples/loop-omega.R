
prevWd <- getwd()
setwd("../src/")
source("./import.R")
setwd(prevWd)


## Example on running different instances with different omega parameters and
## drawing proportions of risk groups and total that are infected over time to
## different files. Also outputs the end states in a table.

main <- function() {

    ## Old parameters.
    ## EmaShape <- 0.52677
    ## EmaScale <- 4.31406

    ## prepare free parameters
    Ema2Shape <- .758
    Ema2Scale <- 5.435
    initialFractionAcute <- 0.001

    omegas <- seq(0, 1, by=.1)

    ## 8 columns: 1 omega + 6 risk groups + 1 total
    columns <- 8
    out <- matrix(rep(NA),
                  ncol=columns,
                  nrow=length(omegas))
    colnames(out) <- c("omega",
                       mapply(toString, c(1:(columns-2))),
                       "All")

    for (i in 1:length(omegas)) {

        w <- omegas[i]

        ## set parameters
        params <- defaultParams
        params <- generateParams(params, Ema2Shape, Ema2Scale, 0, w)
        initState <- initialState(params, initialFractionAcute)

        ## run the model. runInstance provides the initial state of
        ## 0.1% acutely infected in each risk group
        timesteps <- linspace(0, 1000, 100)
        result <- runInstance(timesteps, initState, params)

        ## find infected proportions for each timestep
        props <- proportionInfectedOrTreated(result)

        filename <- paste0("test2-omega-", w, ".png")
        print(filename)
        png(filename)

        ## plot proportions
        plotData(result$t, props)

        dev.off()

        ## get the state at the end, prepend with omega
        numTimesteps <- dim(props)[1]
        out[i,] <- c(w, props[numTimesteps,])
    }
    ## write endstates to file
    write.table(out, file="shape-.758-scale-5.435.csv")
}

main()
