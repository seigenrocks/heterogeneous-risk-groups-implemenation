
### Author: Gijsbert Schröder
### Contact: gijsschroder at gmail dot com
### License: GPL3 (see LICENSE.txt)

### This file contains functions for plotting results. Important
### exports:
###
### plotData <- function(xs, yss, ...)
###     Given x coordinates and 1D or 2D y coordinates, plot one plot
### for each column in y (if 2D, only one plot if 1D) and try to name
### the plots appropriately. Also draw a legend.


## Plot some collection of lines with 1D x coordinates and
## 1D or 2D y coordinates, including a legenda
plotData <- function(xs, yss, names=NA, type="l", loc="topright", ylim=NULL) {

    ## find some suitable names
    names <- if (identical(names, NA)) { #if not given then make up some
                 myss <- as.matrix(yss)
                 if (!identical(colnames(myss), NULL)) {
                     colnames(myss)     #if there already are column names, use those
                 } else {
                     ## if all else fails, just number the sets
                     dims <- dim(as.matrix(yss))[2]
                     mapply(toString, 1:dims)
                 }
             } else {                   # if names are passed just use those
                 names
             }
    
    if (!identical(ylim, NULL)) {
        
        if (length(ylim) != 2) {
            stop("Length of ylim should be 2")
        }
        ## if element is NA, substitute with minimum or maximum of supplied data
        ## depending on element position
        if (identical(ylim[1], NA)) {
            ylim[1] <- min(yss)
        }
        if (identical(ylim[2], NA)) {
            ylim[2] <- max(yss)
        }
    }
    
    range <- c(1:length(names))
    
    matplot(xs, yss, type=type, lty=range, col=range, ylim=ylim)

    legend(loc, legend=names, lty=range, col=range)
}
